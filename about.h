#ifndef ABOUT_H
#define ABOUT_H

#include "About_global.h"

extern "C" ABOUT_EXPORT void about();

#endif // ABOUT_H
